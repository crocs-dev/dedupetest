import csv
import cloudinary.uploader
import logging
from concurrent.futures import ThreadPoolExecutor as PoolExecutor
import re
import sys
import os
import time
import datetime
import json

# Fourth file to run
#
# Reads product-img-url-simple-list.csv which is the output of generate-scene7-img-url-list.py
# For each url, pushes media to Cloudinary, only intended for use with images not tested with video.
#
# Requires:
# pip3 install cloudinary
# Assumes Cloudinary credentials in env, before executing script run the following with your accounts values:
#
# export CLOUDINARY_CLOUD_NAME=""
# export CLOUDINARY_API_KEY=""
# export CLOUDINARY_API_SECRET=""
#
# script based off https://gist.github.com/akshay-ranganath/07617a2420478dbf828782784bd0f6ae
# more info at:
# https://github.com/cloudinary/pycloudinary
# https://cloudinary.com/documentation/django_image_and_video_upload

# Without this the error gets quietly written to the log, I'd rather it be loud and fail fast
if 'CLOUDINARY_CLOUD_NAME' not in os.environ or 'CLOUDINARY_API_KEY' not in os.environ or 'CLOUDINARY_API_SECRET' not in os.environ:
    raise RuntimeError("You don't have all the environment variables set") from None

startTime = time.time()
inputfilepath = '../output/product-img-url-simple-list.csv'
loggingfilepath = '../output/push-images-to-cloudinary-log.csv'
foldername = ''
if len(sys.argv) > 1:
    print('using arg '+sys.argv[1])
    foldername = sys.argv[1]
else:
    # TODO change this variables value or pass arg when executing
    foldername = ''

resourcetype = ''
if len(sys.argv) > 2:
    print('using arg '+sys.argv[2])
    resourcetype = sys.argv[2]
else:
    # TODO change this variables value or pass arg when executing
    resourcetype = 'image'

if os.path.isfile(loggingfilepath):
    os.remove(loggingfilepath)

logging.basicConfig(level=logging.INFO,
    format='%(levelname)s,%(message)s',
    filename=loggingfilepath,
    filemode='a+')

# turn off warnings from urllib3 library
logging.getLogger("urllib3").setLevel(logging.ERROR)
logging.getLogger("cloudinary").setLevel(logging.ERROR)

def remove_extension(file_name):
    # by default, assume file has no extension
    file_without_extension = file_name

    pattern = re.compile(r'(.+)(\.[a-z]{3})')
    pattern_match = pattern.match(file_name)
    if pattern_match and len(pattern_match.groups())==2:
        file_without_extension = pattern_match.group(1)
    return file_without_extension

def gettags(pidcidalt):
    pcalist = pidcidalt.split('_')
    if len(pcalist) == 3:
        taglist = pcalist.copy()
        taglist.append(pidcidalt)
        taglist.append(pcalist[0]+'_'+pcalist[1])
        taglist.append(pcalist[1]+'_'+pcalist[2])
        return taglist
    else:
        return ''

def getpublicid(foldername,pidcidalt):
    if str(foldername).strip() != '':
        return foldername +'/'+ pidcidalt
    else:
        return pidcidalt

def migrate_resources(row):
    if row and len(row) > 0 and str(row[0]).strip() != '':
        cleanrow0 = str(row[0]).strip()
        pidcidalt = cleanrow0.replace('https://images.crocs.com/is/image/Crocs/','')
        pidcidalt = pidcidalt.replace('?scl=1&qlt=100&format=alpha-png','')
        # push with overwrite off
        # if fails due to existing image
        # get existing image width
        # try to push again, use eval of: if this resources width is greater than pre-existing upload_options['overwrite'] = true
        try:
            resp1 = cloudinary.uploader.upload(cleanrow0,
                # Folder/File Name = Public id / identifier for the object on Cloudinary
                # cloudinary does not need extension for images
                public_id = getpublicid(foldername,pidcidalt),
                # If you include foldername I don't think you need to add it to the public_id above,
                # which is confusing since it is part of the public_id
                #folder = foldername,
                unique_filename = False,
                type = 'upload',
                resource_type = resourcetype,
                overwrite = False, # need to be careful of this but SiteCore should use this
                invalidate = True, # required to use
                # TODO Asset ContentHub ID should also be added as a tag
                tags = gettags(pidcidalt),
                # for testing:
                context = {
                    'alt': 'my alt',
                    'caption': 'my caption'
                },
                # !!!WARNING!!! the structured data below must already be set up in the account prior to pushing
                metadata =  {
                    'content_hub_id': '1234',
                    'model_name': 'test'
                }
            )
            print('resp1')
            print(resp1)
            if resp1['existing'] == True:
                print('media was existing, with a width of: '+str(resp1['width']))
                logging.info(f"Found existing {cleanrow0}, trying again if larger...")
                resp2 = cloudinary.uploader.upload(cleanrow0,
                    # Folder/File Name = Public id / identifier for the object on Cloudinary
                    # cloudinary does not need extension for images
                    public_id = getpublicid(foldername,pidcidalt),
                    # If you include foldername I don't think you need to add it to the public_id above,
                    # which is confusing since it is part of the public_id
                    #folder = foldername,
                    unique_filename = False,
                    type = 'upload',
                    resource_type = resourcetype,
                    overwrite = False, # need to be careful of this but SiteCore should use this
                    # ammend overwrite if larger
                    eval = "if (resource_info.width > "+str(resp1['width'])+") { upload_options['overwrite'] = true }",
                    invalidate = True, # required to use
                    # TODO Asset ContentHub ID should also be added as a tag
                    tags = gettags(pidcidalt),
                    # for testing:
                    context = {
                        'alt': 'my alt',
                        'caption': 'my caption'
                    },
                    # !!!WARNING!!! the structured data below must already be set up in the account prior to pushing
                    metadata =  {
                        'content_hub_id': '1234',
                        'model_name': 'test'
                    }
                )
                print('resp2')
                print(resp2)
            logging.info(f"{cleanrow0}")
        except Exception as e:
            logging.error(f"{cleanrow0},{e}")

if __name__=="__main__":
    with open(inputfilepath) as f:
        rows = csv.reader(f)
        # TODO ask if we can increase this
        # run 20 concurrent uploads
        with PoolExecutor(max_workers=20) as executor:
            # migrate images
                for _ in executor.map(migrate_resources, rows):
                    pass
    timestr = str(datetime.timedelta(seconds=time.time() - startTime))
    print('Done! Took ' + timestr)