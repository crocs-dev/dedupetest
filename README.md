To run:

Set environment variables.

```cd path/to/dedupeTest/scripts```

```python3 push-images-to-cloudinary-dedupe.py 'products' 'image'```

Note:  push-images-to-cloudinary-dedupe.py will use URL listed in product-img-url-simple-list.csv to upload to Cloudinary account.